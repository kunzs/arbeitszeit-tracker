import { Component } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { CommonModule, NgStyle } from '@angular/common';
import { SettingsComponent } from './ui/components/settings/settings.component';
import { ActionButtonsComponent } from './ui/components/action-buttons/action-buttons.component';
import { TimesComponent } from './ui/components/times/times.component';
import { ResultsComponent } from './ui/components/results/results.component';
import { NewVersionComponent } from './ui/components/new-version/new-version.component';
import { FormsModule } from '@angular/forms';
import { TrackerDataService } from './data-access/tracker-data.service';

@Component({
    selector: 'app-root',
    standalone: true,
    imports: [
        CommonModule,
        SettingsComponent,
        ActionButtonsComponent,
        TimesComponent,
        ResultsComponent,
        NewVersionComponent,
        FormsModule,
        NgStyle,
    ],
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    animations: [
        trigger('fadeInOut', [
            transition(':enter', [
                style({ opacity: '0' }),
                animate('500ms ease-in', style({ opacity: '1' })),
            ]),
            transition(':leave', [
                animate('500ms ease-in', style({ opacity: '0' })),
            ]),
        ]),
        trigger('slideInOut', [
            transition(':enter', [
                style({ transform: 'translateY(-100%)' }),
                animate(
                    '300ms ease-in-out',
                    style({ transform: 'translateY(0)' })
                ),
            ]),
            transition(':leave', [
                animate(
                    '300ms ease-in-out',
                    style({ transform: 'translateY(-100%)' })
                ),
            ]),
        ]),
    ],
})
export class AppComponent {
    public input = '';

    constructor(public data: TrackerDataService) {}

    public add() {
        // Haben wir genau 4 Stellen?
        if (this.input.length !== 4) {
            return;
        }

        // Haben wir nur Ziffern?
        if (!this._onlyDigits(this.input)) {
            this.input = '';
            return;
        }

        // In Stunden und Minuten umwandeln
        const inputHours = parseInt(this.input[0] + this.input[1], 10);
        const inputMinutes = parseInt(this.input[2] + this.input[3], 10);

        // Checken dass es sich um eine valide Uhrzeit handelt
        if (
            inputHours > 23 ||
            inputHours < 0 ||
            inputMinutes > 59 ||
            inputMinutes < 0
        ) {
            this.input = '';
            return;
        }
        this.data.add(inputHours, inputMinutes);
        this.input = '';
    }

    private _onlyDigits(input: string): boolean {
        for (const char of input) {
            if (isNaN(parseFloat(char))) {
                return false;
            }
        }

        return true;
    }
}
