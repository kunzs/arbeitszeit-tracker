import { Injectable } from '@angular/core';
import { Settings } from '../models';

@Injectable({
    providedIn: 'root',
})
export class PersistenceService {
    public saveTimeEntries(timeEntries: Array<number>): void {
        window.localStorage.setItem('entries', JSON.stringify(timeEntries));
    }

    public loadTimeEntries(): Array<number> {
        try {
            const loaded = window.localStorage.getItem('entries');
            if (!loaded) {
                return [];
            }
            return JSON.parse(loaded);
        } catch {
            return [];
        }
    }

    public saveSettings(settings: Settings): void {
        window.localStorage.setItem('settings', JSON.stringify(settings));
    }

    public loadSettings(): Settings | null {
        try {
            const loaded = window.localStorage.getItem('settings');
            if (!loaded) {
                return null;
            }
            return JSON.parse(loaded);
        } catch {
            return null;
        }
    }

    public saveHideNewVersionInfo(newVersionKey: string, _: boolean): void {
        window.localStorage.setItem(
            `newVersion_${newVersionKey}`,
            JSON.stringify(true)
        );
    }

    public loadHideNewVersionInfo(newVersionKey: string): boolean {
        try {
            const loaded = window.localStorage.getItem(
                `newVersion_${newVersionKey}`
            );
            if (!loaded) {
                return false;
            }
            return JSON.parse(loaded);
        } catch {
            return false;
        }
    }
}
