import { Injectable } from '@angular/core';
import { Result, lawRequirements, Period, Settings } from '../models';

@Injectable({
    providedIn: 'root',
})
export class CalculationService {
    public calculate(
        timeEntries: Array<number>,
        currentTime: number,
        settings: Settings
    ) {
        const result = new Result();

        this._setPeriods(timeEntries, currentTime, result);
        this._setPresentTime(result, settings);
        this._setMinBreakTime(result);
        this._setLeftBreakTime(result);
        this._setOvertime(result, settings);
        this._setEarliestEndOfDay(timeEntries, result, settings);

        return result;
    }

    private _setPeriods(
        timeEntries: Array<number>,
        currentTime: number,
        result: Result
    ): void {
        timeEntries = JSON.parse(JSON.stringify(timeEntries));

        if (timeEntries.length % 2 === 1) {
            timeEntries.push(currentTime);
        }

        for (let i = 0; i < timeEntries.length - 1; i++) {
            if (i % 2 === 0) {
                // Present
                result.presentPeriods.push(
                    new Period(timeEntries[i], timeEntries[i + 1])
                );
            } else {
                // Break
                result.breakPeriods.push(
                    new Period(timeEntries[i], timeEntries[i + 1])
                );
            }
        }
    }

    private _setPresentTime(result: Result, settings: Settings): void {
        result.presentTime = 0;
        for (const presentPeriod of result.presentPeriods) {
            result.presentTime += presentPeriod.duration;
        }
        result.presentTimeAfterWayTime = result.presentTime - settings.wayTime;
    }

    private _setMinBreakTime(result: Result): void {
        if (result.presentTimeAfterWayTime <= lawRequirements.noBreaksTill) {
            result.minBreakTime = 0;
        } else if (
            result.presentTimeAfterWayTime <= lawRequirements.smallBreakTill
        ) {
            result.minBreakTime = lawRequirements.smallBreak;
        } else {
            result.minBreakTime = lawRequirements.bigBreak;
        }
    }

    private _setLeftBreakTime(result: Result): void {
        result.leftBreakTime = result.minBreakTime;
        result.breakTimeForMinBreak = 0;
        result.breakTimeExtra = 0;

        for (const breakPeriod of result.breakPeriods) {
            if (breakPeriod.duration >= lawRequirements.minBreakTimeForLaw) {
                result.breakTimeForMinBreak += breakPeriod.duration;
            } else {
                result.breakTimeExtra += breakPeriod.duration;
            }
        }

        result.leftBreakTime =
            result.minBreakTime - result.breakTimeForMinBreak;

        if (result.leftBreakTime < 0) {
            result.leftBreakTime = 0;
        }
    }

    private _setOvertime(result: Result, settings: Settings): void {
        result.overtime =
            result.presentTime -
            settings.wayTime -
            settings.workTime -
            result.leftBreakTime;
    }

    private _setEarliestEndOfDay(
        timeEntries: Array<number>,
        result: Result,
        settings: Settings
    ): void {
        let endOfWorkingDayMillis;

        if (result.overtime >= 0) {
            result.earliestEndOfWorkingDay = null;
            return;
        }

        // Anfang des Arbeitstags
        endOfWorkingDayMillis = timeEntries[0];

        // Arbeitsstunden draufrechnen
        endOfWorkingDayMillis += settings.workTime;

        // Wegzeit draufrechnen
        endOfWorkingDayMillis += settings.wayTime;

        // mindestpause oder pause (wenn schon >= mindestpause) draufrechnen
        if (
            result.minBreakTime === 0 &&
            result.breakTimeForMinBreak < lawRequirements.smallBreak
        ) {
            endOfWorkingDayMillis += lawRequirements.smallBreak;
        } else if (result.breakTimeForMinBreak >= result.minBreakTime) {
            endOfWorkingDayMillis += result.breakTimeForMinBreak;
        } else {
            endOfWorkingDayMillis += result.minBreakTime;
        }

        // Kurze Pausen draufrechnen
        endOfWorkingDayMillis += result.breakTimeExtra;

        result.earliestEndOfWorkingDay = new Date(endOfWorkingDayMillis);
    }
}
