import { Injectable, computed, effect, signal } from '@angular/core';
import { interval, map, startWith } from 'rxjs';
import { Settings } from '../models';
import { PersistenceService } from './persistence.service';
import { CalculationService } from './calculation.service';

@Injectable({
    providedIn: 'root',
})
export class TrackerDataService {
    public settings = signal<Settings>(new Settings());

    public settingsActive = signal<boolean>(false);

    public showNewVersionInfo = signal<boolean>(false);

    public currentTime = signal(0);

    public timeEntries = signal<Array<number>>([]);

    public result = computed(() => {
        return this._calculationService.calculate(
            this.timeEntries(),
            this.currentTime(),
            this.settings()
        );
    });

    private _newVersionKey = 'farbschema';

    private _settingsSave: Settings | null = null;

    // -------------------------------------------------------------------
    // I N I T I A L I Z A T I O N
    // -------------------------------------------------------------------
    constructor(
        private _persistanceService: PersistenceService,
        private _calculationService: CalculationService
    ) {
        this._initSettingsPersistance();
        this._initNewVersionInfoPersistance();
        this._initTimeEntriesPersistance();
        this._initCurrentTimeInterval();
    }

    private _initSettingsPersistance() {
        // Load Settings
        const savedSettings = this._persistanceService.loadSettings();
        if (savedSettings) {
            this.settings.set(savedSettings);
        } else {
            this._persistanceService.saveSettings(this.settings());
        }

        // Save Settings
        effect(() => {
            this._persistanceService.saveSettings(this.settings());
        });
    }

    private _initTimeEntriesPersistance() {
        // Load Time Entries
        this.timeEntries.set(this._persistanceService.loadTimeEntries());

        // Save Time Entries
        effect(() => {
            this._persistanceService.saveTimeEntries(this.timeEntries());
        });
    }

    private _initNewVersionInfoPersistance() {
        // Load Version Info Shown
        this.showNewVersionInfo.set(
            !this._persistanceService.loadHideNewVersionInfo(
                this._newVersionKey
            )
        );

        // Save Version Info Shown
        effect(() => {
            this._persistanceService.saveHideNewVersionInfo(
                this._newVersionKey,
                this.showNewVersionInfo()
            );
        });
    }

    private _initCurrentTimeInterval() {
        interval(4000)
            .pipe(
                startWith(0),
                map((x) => {
                    return this._getNormalizedTime().getTime();
                })
            )
            .subscribe((current) => {
                this.currentTime.set(current);
            });
    }

    // -------------------------------------------------------------------
    // P U B L I C   F U N C T I O N S
    // -------------------------------------------------------------------

    public hideNewVersionInfo(): void {
        this.showNewVersionInfo.set(false);
    }

    public showSettings() {
        this._settingsSave = Object.assign({}, this.settings());
        this.settingsActive.set(true);
    }

    public setWorkTime(workTime: number) {
        this.settings.mutate((settings) => {
            settings.workTime = workTime;
        });
    }

    public setWayTime(wayTime: number) {
        this.settings.mutate((settings) => {
            settings.wayTime = wayTime;
        });
    }

    public setMaxPresentTime(maxPresentTime: number) {
        this.settings.mutate((settings) => {
            settings.maxPresentTime = maxPresentTime;
        });
    }

    public setThemeColor(color: string) {
        this.settings.mutate((settings) => {
            settings.color = color;
        });
    }

    public saveSettings(): void {
        this._persistanceService.saveSettings(this.settings());
        this.settingsActive.set(false);
    }

    public cancelSettings(): void {
        this.settings.set(this._settingsSave!);
        this.settingsActive.set(false);
    }

    public add(hours: number, minutes: number) {
        const entryDate = this._getNormalizedTime();
        entryDate.setHours(hours);
        entryDate.setMinutes(minutes);
        this._add(entryDate.getTime());
    }

    public addNow() {
        this._add(this._getNormalizedTime().getTime());
    }

    public remove(timestamp: number) {
        this.timeEntries.update((timeEntries) => {
            return timeEntries.filter((x) => x !== timestamp);
        });
    }

    public reset() {
        this.timeEntries.set([]);
    }

    // -------------------------------------------------------------------
    // H E L P E R S
    // -------------------------------------------------------------------
    private _getNormalizedTime() {
        const date = new Date();
        date.setFullYear(2000);
        date.setMonth(0);
        date.setDate(1);
        date.setSeconds(0);
        date.setMilliseconds(0);
        return date;
    }

    private _add(timestamp: number) {
        this.timeEntries.mutate((timeEntries) => {
            if (timeEntries.indexOf(timestamp) >= 0) {
                return;
            }
            timeEntries.push(timestamp);
            timeEntries.sort((a, b) => {
                return a - b;
            });
        });
    }
}
