export const lawRequirements = {
    noBreaksTill: 6 * 60 * 60 * 1000,
    smallBreakTill: 9 * 60 * 60 * 1000,
    smallBreak: 30 * 60 * 1000,
    bigBreak: 45 * 60 * 1000,
    minBreakTimeForLaw: 15 * 60 * 1000,
};

export class Settings {
    workTime: number;
    wayTime: number;
    maxPresentTime: number;
    color: string;

    constructor() {
        this.workTime = 28800000;
        this.wayTime = 300000;
        this.maxPresentTime = 37800000;
        this.color = '#008888';
    }
}

export class Period {
    start: Date;
    end: Date;

    constructor(start: number, end: number) {
        this.start = new Date(start);
        this.end = new Date(end);
    }

    public get duration() {
        return this.end.getTime() - this.start.getTime();
    }
}

export class Result {
    presentPeriods: Array<Period>;
    breakPeriods: Array<Period>;

    presentTime: number;
    presentTimeAfterWayTime: number;
    overtime: number;

    minBreakTime: number;
    leftBreakTime: number;

    breakTimeForMinBreak: number;
    breakTimeExtra: number;

    earliestEndOfWorkingDay: Date | null;

    presentTooLong: boolean;

    constructor() {
        this.presentPeriods = [];
        this.breakPeriods = [];

        this.presentTime = 0;
        this.presentTimeAfterWayTime = 0;
        this.overtime = 0;

        this.minBreakTime = 0;
        this.leftBreakTime = 0;

        this.breakTimeForMinBreak = 0;
        this.breakTimeExtra = 0;

        this.earliestEndOfWorkingDay = new Date();

        this.presentTooLong = false;
    }
}
