import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class UiHelperService {
    public minutesToMillis(minutes: number) {
        return minutes * 60 * 1000;
    }

    public hoursToMillis(hours: number) {
        return hours * 60 * 60 * 1000;
    }
}
