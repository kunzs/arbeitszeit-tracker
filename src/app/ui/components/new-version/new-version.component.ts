import { Component, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-new-version',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './new-version.component.html',
  styleUrls: ['./new-version.component.scss']
})
export class NewVersionComponent {
    @Output()
    public jumpToSettings = new EventEmitter<void>();

    @Output()
    public hideNewVersionNews = new EventEmitter<void>();
}
