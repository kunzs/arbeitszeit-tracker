import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MillisToTimeStringPipe } from '../../pipes/millis-to-time-string.pipe';
import { MillisToTimeSpanStringPipe } from '../../pipes/millis-to-time-span-string.pipe';
import { Result, Settings } from 'src/app/models';

@Component({
    selector: 'app-results',
    standalone: true,
    imports: [CommonModule, MillisToTimeStringPipe, MillisToTimeSpanStringPipe],
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.scss'],
})
export class ResultsComponent {
    @Input()
    public timeEntries: Array<number> = [];

    @Input()
    public result: Result | null = null;

    @Input()
    public settings: Settings | null = null;
}
