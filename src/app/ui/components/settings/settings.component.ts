import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiHelperService } from '../../services/ui-helper.service';
import { FormsModule } from '@angular/forms';
import { Settings } from 'src/app/models';
import { MillisToHoursPipe } from '../../pipes/millis-to-hours.pipe';
import { MillisToMinutesPipe } from '../../pipes/millis-to-minutes.pipe';

@Component({
    selector: 'app-settings',
    standalone: true,
    imports: [
        CommonModule,
        FormsModule,
        MillisToHoursPipe,
        MillisToMinutesPipe,
    ],
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent {
    @Input()
    public settings: Settings | null = null;

    @Output()
    public saveSettings = new EventEmitter<void>();

    @Output()
    public cancelSettings = new EventEmitter<void>();

    @Output()
    public setWorkTime = new EventEmitter<number>();

    @Output()
    public setWayTime = new EventEmitter<number>();

    @Output()
    public setMaxPresentTime = new EventEmitter<number>();

    @Output()
    public setThemeColor = new EventEmitter<string>();

    constructor(public uiHelper: UiHelperService) {}
}
