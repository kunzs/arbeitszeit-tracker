import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-action-buttons',
    standalone: true,
    templateUrl: './action-buttons.component.html',
    styleUrls: ['./action-buttons.component.scss'],
})
export class ActionButtonsComponent {
    @Output()
    public showSettings = new EventEmitter<void>();

    @Output()
    public addNow = new EventEmitter<void>();

    @Output()
    public reset = new EventEmitter<void>();
}
