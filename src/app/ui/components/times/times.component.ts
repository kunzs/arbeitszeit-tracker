import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MillisToTimeStringPipe } from '../../pipes/millis-to-time-string.pipe';

@Component({
    selector: 'app-times',
    standalone: true,
    imports: [CommonModule, MillisToTimeStringPipe],
    templateUrl: './times.component.html',
    styleUrls: ['./times.component.scss'],
})
export class TimesComponent {
    @Input()
    public timeEntries: Array<number> = [];

    @Input()
    public currentTime = 0;

    @Output()
    public removeEntry = new EventEmitter<number>();
}
