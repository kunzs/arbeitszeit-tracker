import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'millisToMinutes',
    standalone: true,
})
export class MillisToMinutesPipe implements PipeTransform {
    transform(millis: number): number {
        return millis / 1000 / 60;
    }
}
