import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'millisToHours',
    standalone: true,
})
export class MillisToHoursPipe implements PipeTransform {
    transform(millis: number): number {
        return millis / 1000 / 60 / 60;
    }
}
