import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'millisToTimeString',
    standalone: true
})
export class MillisToTimeStringPipe implements PipeTransform {

    transform(value: number): any {

        if (!value) {
            return '';
        }

        return moment(value).format('HH:mm');
    }

}
